import React from 'react'
import fetch from 'isomorphic-unfetch'

import "../Styles/Pages/Legislators.scss"

import { LegislatorsTable, Leadership } from "../Components/Legislators";
import PageIntro from '../Components/PageIntro';

import legislatorsPageData from "../SampleData/legislatorsPageData"

const LegislatorsPage = ({ legislators }) => {
  return (
    <>
      <section className="container">
        <PageIntro
          heading={legislatorsPageData.heading}
          description={legislatorsPageData.description}
          hasSidebar={true}
          sidebarContent={<Leadership legislators={legislators} />}
        />
      </section>
      <section className="container section">
        <LegislatorsTable
          title="Legislators"
          headings={["Title", "Name", "District", "Party", "Capitol Phone #", "Email"]}
          legislators={legislators}
        />
      </section>
    </>
  )
}

LegislatorsPage.getInitialProps = async () => {
  const res = await fetch('http://cogar.denvertech.org/legislators.json')
  const json = await res.json();
  return { legislators: json }
}

export default LegislatorsPage
