import React from 'react'

import PageIntro from "../Components/PageIntro"
import SessionPage from '../Components/SessionPage/SessionPage'

const SessionSchedule = () => {
  return (
    <>
      <section className="container">
        <PageIntro
          heading={"Session Schedule"}
          description={(
            <>
              The Second Regular Session of the Seventy-second General Assembly convened on January 8th, 2020.  The House and Senate generally convene at 10:00 a.m. on Mondays and 9:00 a.m. on Tuesdays through Fridays.  Committees may meet upon adjournment of their respective chambers, at a time set in the chamber calendar, or as scheduled by the committee chair.  The official calendars may be accessed below.
              <br />
              <br />
              <ul className="inline-list">
                <li>
                  <a className="button default secondary" href="/">
                    House Calendar
                  </a>
                </li>
                <li>
                  <a className="button default secondary" href="/">
                    Senate Calendar
                  </a>
                </li>
              </ul>
            </>
          )}
          hasSidebar={false}
        />
      </section>
      <section className="container section">
        <SessionPage />
      </section>
    </>
  )
}

export default SessionSchedule
