// import App from 'next/app'

// Fonts
// import "typeface-merriweather";
// import "typeface-public-sans";
import "../Styles/Application.scss"

import useWindowSize from "../Hooks/useWindowSize";
import WindowContext from "../Context/WindowContext";

import { DefaultLayout as Layout } from "../Layout";

function MyApp({ Component, pageProps }) {
  const [width, height] = useWindowSize();

  return (
    <WindowContext.Provider value={{ width, height }}>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </WindowContext.Provider>
  )
}

// Only uncomment this method if you have blocking data requirements for
// every single page in your application. This disables the ability to
// perform automatic static optimization, causing every page in your app to
// be server-side rendered.
//
// MyApp.getInitialProps = async (appContext) => {
//   // calls page's `getInitialProps` and fills `appProps.pageProps`
//   const appProps = await App.getInitialProps(appContext);
//
//   return { ...appProps }
// }

export default MyApp
