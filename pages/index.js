import HeroCarousel from "../Components/HeroCarousel";
import QuickLinks from "../Components/QuickLinks";
import Block from "../Components/Block";
import LineItem from "../Components/LineItem";
import Schedule from "../Components/Schedule";

const Home = () => (
  <>
    <section className="container container--full section">
      <HeroCarousel />
    </section>

    <section className="container section">
      <QuickLinks />
    </section>

    <section className="container section">
      <Schedule />
    </section>

    <section className="container section text-align-center">
      <div className="text-align-center">
        <h2 className="h2 m-xsmall-bottom-4">Status Sheets & Journals</h2>
        <section className="grid grid-medium-2">
          <div className="col col-xsmall-12 col-medium-6 flex flex--column align-items-center">
            <h3 className="h3 m-xsmall-bottom-2">Status of Bills:</h3>
            <ul className="inline-list">
              <li>
                <a className="button default tertiary" href="/" target="_blank" rel="noopener noreferrer">
                  View House Status Sheet
                </a>
              </li>
              <li>
                <a className="button default tertiary" href="/" target="_blank" rel="noopener noreferrer">
                  View Senate Status Sheet
                </a>
              </li>
            </ul>
          </div>
          <div className="col col-xsmall-12 col-medium-6 flex flex--column align-items-center">
            <h3 className="h3 m-xsmall-bottom-2">Cumulative Journals:</h3>
            <ul className="inline-list">
              <li>
                <a className="button default tertiary" href="/" target="_blank" rel="noopener noreferrer">
                  View House Status Sheet
                </a>
              </li>
              <li>
                <a className="button default tertiary" href="/" target="_blank" rel="noopener noreferrer">
                  View Senate Status Sheet
                </a>
              </li>
            </ul>
          </div>
        </section>
      </div>
    </section>
    <section className="container section grid grid-medium-2">
      <Block title="Top Bills" cta={{url: '', title: 'Browse All Bills'}}>
        <LineItem link="/" subtitle="HB20-1272" title="Colorado Natural Marriage And  Adoption Act" />
        <LineItem link="/" subtitle="HB20-1272" title="Colorado Natural Marriage And  Adoption Act" />
        <LineItem link="/" subtitle="HB20-1272" title="Colorado Natural Marriage And  Adoption Act" />
      </Block>
      <Block title="Top Publications" cta={{url: '/', title: 'Browse All Publications'}}>
        <LineItem link="/" subtitle="HB20-1272" title="Colorado Natural Marriage And  Adoption Act" />
        <LineItem link="/" subtitle="HB20-1272" title="Colorado Natural Marriage And  Adoption Act" />
        <LineItem link="/" subtitle="HB20-1272" title="Colorado Natural Marriage And  Adoption Act" />
      </Block>
    </section>
  </>
)

export default Home
