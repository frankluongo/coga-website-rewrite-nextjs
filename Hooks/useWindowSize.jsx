import { useEffect, useState } from 'react';

import { debounce } from "lodash"

function useWindowSize() {
  const [size, setSize] = useState([0, 0]);

  useEffect(() => {

    function updateSize() {
      setSize([window.innerWidth, window.innerHeight]);
    }

    window.addEventListener('resize', debounce(updateSize, 250));

    updateSize();

    return () => window.removeEventListener('resize', updateSize);

  }, []);

  return size;
}

// Example Function

// function ShowWindowDimensions(props) {
//   const [width, height] = useWindowSize();
//   return <span>Window size: {width} x {height}</span>;
// }

export default useWindowSize
