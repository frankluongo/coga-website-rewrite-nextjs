import React from 'react'

import Footer from "../Components/Footer"
import NotificationBar from '../Components/NotificationBar'
import Header from '../Components/Header'

const DefaultLayout = ({ children, displayNotification }) => {
  return (
    <>
      {displayNotification && <NotificationBar />}
      <Header />
      <main id="main-content">
        {children}
      </main>
      <Footer />
    </>
  )
}

DefaultLayout.defaultProps = {
  displayNotification: true,
}

export default DefaultLayout
