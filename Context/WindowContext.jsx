import { createContext } from "react";

const defaultValues = {
  width: 0,
  height: 0
}

const WindowContext = createContext(defaultValues)

export default WindowContext
