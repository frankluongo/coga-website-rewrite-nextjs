import React from "react"

const legPageData = {
  heading: "Legislators",
  description: (
    <>
      The General Assembly consists of 100 members - 35 Senators and 65 Representatives.  Senators serve four-year terms, while Representatives serve two-year terms.  All members are limited to serving for eight consecutive years in their chamber - four terms for Representatives and two terms for Senators.  As of the 2010 census, State Senators serve an average of 143,691 residents and State Representatives serve an average of 77,372 residents.
      <br />
      <br />
      The President of the Senate, Speaker of the House of Representatives, and the Majority and Minority Leaders of each chamber serve as the primary leadership for the legislature.  These six members are responsible for the day-to-day operations of the House and Senate as well as serving as the oversight authority for the legislative service agencies.  Each house elects additional leadership positions as well
      <br />
      <br />
      <a
        href="http://leg.colorado.gov/sites/default/files/memberdirectory2020.xlsx"
        type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; length=20322"
        className="button primary default"
        target="_blank"
        title="Member Directory (Updated on 1/9/2020)"
      >
        Download Member Directory (updated 1/9/2020)
      </a>
    </>
  )
}

export default legPageData
