import React from 'react'

import "./Block.scss"

const Block = ({ children, cta, title }) => {
  return (
    <section className="block">
      <div className="block__title h2 small">
        {title}
      </div>
      <div className="block__content">
        {children}
      </div>
      <a className="block__button button default primary" href={cta.url}>
        {cta.title}
      </a>
    </section>
  )
}

export default Block
