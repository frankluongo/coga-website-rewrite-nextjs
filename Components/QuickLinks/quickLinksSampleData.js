import React from "react"
import FindMyLegislator from "../Illustrations/FindMyLegislator"

const quickLinksSampleData = {
  heading: 'Quick Links',
  subHeading: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit.',
  links: [
    {
      icon: (
        <>
          <FindMyLegislator />
        </>
      ),
      url: '/',
      title: 'Quick Link Title'
    },
    {
      icon: (
        <>
          <FindMyLegislator />
        </>
      ),
      url: '/',
      title: 'Quick Link Title'
    },
    {
      icon: (
        <>
          <FindMyLegislator />
        </>
      ),
      url: '/',
      title: 'Quick Link Title'
    },
    {
      icon: (
        <>
          <FindMyLegislator />
        </>
      ),
      url: '/',
      title: 'Quick Link Title'
    },
  ]
}

export default quickLinksSampleData
