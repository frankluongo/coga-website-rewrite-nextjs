import React from 'react'

import data from "./quickLinksSampleData"

import "./QuickLinks.scss"
import QuickLink from './QuickLink'

const QuickLinks = ({ data }) => {
  return (
    <section className="quick-links">
      <div className="quick-links__container container">
        <div className="quick-links__text">
          <h2 className="quick-links__heading h2">{data.heading}</h2>
          <p>{data.subHeading}</p>
        </div>
        <ul className="quick-links__links">
          {data.links.map((link, index) => (
            <QuickLink link={link} key={index} />
          ))}
        </ul>
      </div>
    </section>
  )
}

QuickLinks.defaultProps = {
  data
}

export default QuickLinks
