import React from 'react'

import "./QuickLink.scss"

import { IconChevronRight } from '../Icons'

const QuickLink = ({ link }) => {
  return (
    <li className="quick-links__link-item">
      <a className="quick-links__link quick-links-link link" href={link.url}>
        <div className="quick-links-link__illustration">
          {link.icon}
        </div>
        <div className="quick-links-link__title">
          {link.title}
        </div>
        <div className="quick-links-link__arrow">
          <IconChevronRight />
        </div>
      </a>
    </li>
  )
}

export default QuickLink
