import React from 'react'

import { IconSearch } from "../Icons"

import "./SearchForm.scss"

const SearchForm = ({ modifiers, searchId }) => {
  return (
    <section className={`search-form ${modifiers}`}>
      <form
        className="search-form__form"
        action="/search"
        method="POST"
        role="search"
      >
        <label className="visuallyHidden" htmlFor={`q-${searchId}`}>Search Our Website</label>
        <input className="search-form__input" id={`q-${searchId}`} type="search" placeholder="Search" />
        <button className="search-form__button" aria-label="Search Our Website" type="submit">
          <IconSearch />
        </button>
      </form>
    </section>
  )
}

SearchForm.defaultProps = {
  modifiers: "",
  searchId: '1'
}

export default SearchForm
