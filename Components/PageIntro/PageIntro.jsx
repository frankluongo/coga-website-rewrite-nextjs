import React from 'react'

import "./PageIntro.scss"

const PageIntro = ({ description, hasSidebar, heading, sidebarContent }) => {
  return (
    <section className={`page-intro ${hasSidebar ? 'page-intro--with-sidebar' : ''}`}>
      <div className="page-intro__main">
        <h2 className="page-intro-main__heading h1">
          {heading}
        </h2>
        <p className="page-intro-main__description">
          {description}
        </p>
      </div>
      {hasSidebar && (
        <aside className="page-intro__sidebar">
          {sidebarContent}
        </aside>
      )}
    </section>
  )
}

PageIntro.defaultProps = {
  description: "",
  heading: "",
  hasSidebar: false,
  sidebarContent: ""
}

export default PageIntro
