import React from 'react'
import Table, { TableBody, TableHead, TableRow, TableHeading, TableData } from '../Table'

import "./SessionCalendar.scss"
import { IconCalendar, IconVideoCamera } from '../Icons/'

const SessionCalendar = ({ events, link, scheduleType }) => {
  return (
    <div className={`session-calendar session-calendar--${scheduleType}`}>
      <div className="session-calendar__heading h4">
        <div className={`session-calendar-heading__text session-calendar-heading__text--${scheduleType}`}>
          {scheduleType} Schedule
        </div>
        <a className="session-calendar-heading__icon session-calendar-heading__icon--calendar" href="/">
          <IconCalendar />
          <span className="session-calendar-heading-icon__text">Download Calendar</span>
        </a>
        <a className="session-calendar-heading__icon session-calendar-heading__icon--calendar" href="/">
          <IconVideoCamera />
          <span className="session-calendar-heading-icon__text">Watch / Listen</span>
        </a>
      </div>
      <Table>
        <TableHead>
          <TableRow type="head">
            <TableHeading>
              Time
            </TableHeading>
            <TableHeading>
              Event
            </TableHeading>
          </TableRow>
        </TableHead>
        <TableBody>
          {events.map((event, index) => (
            <TableRow type="body" key={index}>
              <TableData>
                {event.time}
              </TableData>
              <TableData>
                {event.event}
              </TableData>
            </TableRow>
          ))}
        </TableBody>
      </Table>

      <a className="session-calendar__full-cal button secondary default" href={link}>
        View Today's Full Schedule
      </a>
    </div>
  )
}

export default SessionCalendar
