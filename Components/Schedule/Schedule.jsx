import React from 'react'

import data from "./placeholderScheduleData"
import SessionCalendar from './SessionCalendar'

import "./Schedule.scss"
import InterimCalendar from './InterimCalendar'

const Schedule = ({ data, isSession }) => {
  return (
    <section className="schedule">
      <div className="schedule__top text-align-center">
        <h2 className="schedule__heading h2">
          {isSession ? `Today's` : 'Interim'} Schedule
        </h2>
        <div className="schedule__dates h3">
          {data.dates}
        </div>
      </div>
      {isSession ? (
        <div className="grid grid-medium-2">
          <SessionCalendar
            events={data.houseEvents}
            scheduleType="house"
          />
          <SessionCalendar
            events={data.houseEvents}
            scheduleType="senate"
          />
        </div>
      ) : (
          <div>
            <InterimCalendar events={data.interimEvents} />
          </div>
        )}

    </section>
  )
}

Schedule.defaultProps = {
  isSession: false,
  data
}

export default Schedule
