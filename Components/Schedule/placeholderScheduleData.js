import React from "react"

const data = {
  dates: "Monday, February 10th 2020",
  houseEvents: [
    {
      time: '10:00 AM',
      event: 'House & Floor Work'
    },
    {
      time: '11:00 AM',
      event: 'House & Floor Work'
    },
  ],
  interimEvents: [
    {
      day: 'Monday',
      events: [
        {
          time: '10:00 AM',
          activity: 'Joint Budget Committee',
          location: 'JBC',
          calendar: (
            <a className="link" href="/">
              Agenda | PDF
            </a>
          ),
          listenIn: (<a className="link" href="/">Listen To Audio</a>)
        }
      ]
    }
  ]
}

export default data
