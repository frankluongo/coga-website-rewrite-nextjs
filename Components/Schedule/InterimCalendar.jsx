import React from 'react'
import Table, { TableBody, TableHead, TableRow, TableHeading, TableData } from '../Table'

import "./InterimCalendar.scss"

const InterimCalendar = ({ events, link, scheduleType }) => {
  return (
    <div className={`session-calendar session-calendar--${scheduleType}`}>
      {events.map((event, index) => (
        <div key={index}>
          <div className="session-calendar__heading h4">
            <div className={`session-calendar-heading__text`}>
              {event.day}
            </div>
          </div>
          <div className="interim-calendar__table">
            <Table>
              <TableHead>
                <TableRow type="head">
                  <TableHeading>
                    Time
              </TableHeading>
                  <TableHeading>
                    Activity
              </TableHeading>
                  <TableHeading>
                    Location
              </TableHeading>
                  <TableHeading>
                    Calendar
              </TableHeading>
                  <TableHeading>
                    Audio
              </TableHeading>
                </TableRow>
              </TableHead>
              <TableBody>
                {event.events.map((dayEvent, index) => (
                  <TableRow type="body" key={index}>
                    <TableData>
                      {dayEvent.time}
                    </TableData>
                    <TableData>
                      {dayEvent.activity}
                    </TableData>
                    <TableData>
                      {dayEvent.location}
                    </TableData>
                    <TableData>
                      {dayEvent.calendar}
                    </TableData>
                    <TableData>
                      {dayEvent.listenIn}
                    </TableData>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </div>
          {event.events.map((dayEvent, index) => (
            <div className="interim-calendar__tile" key={index}>
              <div className="interim-calendar-tile__activity">
                <h4 className="interim-calendar-tile__heading h4 small light">Activity</h4>
                {dayEvent.activity}
              </div>
              <div className="interim-calendar-tile__date-location">
                <div className="interim-calendar-tile__date">
                  <h4 className="interim-calendar-tile__heading h4 small light">Time</h4>
                  {dayEvent.time}
                </div>
                <div className="interim-calendar-tile__location">
                  <h4 className="interim-calendar-tile__heading h4 small light">Location</h4>
                  {dayEvent.location}
                </div>
              </div>
              <div className="interim-calendar-tile__calendar-listen">
                <div className="interim-calendar-tile__date">
                  <h4 className="interim-calendar-tile__heading h4 small light">Calendar</h4>
                  {dayEvent.calendar}
                </div>
                <div className="interim-calendar-tile__location">
                  <h4 className="interim-calendar-tile__heading h4 small light">Listen In</h4>
                  {dayEvent.listenIn}
                </div>
              </div>
            </div>
          ))}
        </div>
      ))}
      <a className="session-calendar__full-cal button secondary default" href={link}>
        View Full Schedule
      </a>
    </div>
  )
}

export default InterimCalendar
