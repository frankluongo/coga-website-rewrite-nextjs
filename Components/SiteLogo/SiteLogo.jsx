import React from 'react'
import Link from "next/link"

import "./SiteLogo.scss"

const SiteLogo = ({ modifiers, siteSubTitle, siteTitle }) => {
  return (
    <div className={`site-logo ${modifiers}`}>
      <Link href="/">
        <a className="site-logo__logo-wrapper">
          <img className="site-logo__logo" src="/cga-logo-med.png" alt="Colorado General Assembly" />
        </a>
      </Link>
      <div className="site-logo__text">
        <div className="site-logo__subtitle">
          {siteSubTitle}
        </div>
        <h1 className="site-logo__site-title h3">
          {siteTitle}
        </h1>
      </div>
    </div>
  )
}

SiteLogo.defaultProps = {
  modifiers: "",
  siteSubTitle: "Second Regular Session | 72nd General Assembly",
  siteTitle: "Colorado General Assembly"
}

export default SiteLogo
