import React from 'react'

import "./Table.scss"

const Table = ({ children }) => {
  return (
    <table className="table" data-table="">
      {children}
    </table>
  )
}

Table.defaultProps = {
  children: "",
}

export default Table
