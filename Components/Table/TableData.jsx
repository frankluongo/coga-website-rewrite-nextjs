import React from 'react'

const TableData = ({ children }) => {
  return (
    <td className="table-body-row__item">
      {children}
    </td>
  )
}

export default TableData
