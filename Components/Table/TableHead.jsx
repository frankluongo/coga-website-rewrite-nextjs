import React from 'react'

const TableHead = ({ children }) => {
  return (
    <thead className="table__head table-head">
      {children}
    </thead>
  )
}

export default TableHead
