import React from 'react'

const TableRow = ({ children, type }) => {
  return (
    <tr className={`table-${type}__row table-${type}-row`}>
      {children}
    </tr>
  )
}

TableRow.defaultProps = {
  type: "head"
}

export default TableRow
