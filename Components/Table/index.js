import Table from "./Table"
import TableBody from "./TableBody"
import TableData from "./TableData"
import TableHead from "./TableHead"
import TableHeading from "./TableHeading"
import TableRow from "./TableRow"

export { Table, TableBody, TableData, TableHead, TableHeading, TableRow }
export default Table
