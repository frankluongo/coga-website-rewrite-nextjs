import React from 'react'

const TableHeading = ({ children }) => {
  return (
    <th className="table-head-row__heading">
      {children}
    </th>
  )
}

export default TableHeading
