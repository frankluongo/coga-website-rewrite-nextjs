import React from 'react'

import "./LineItem.scss"

const LineItem = ({ link, subtitle, title }) => {
  return (
    <a className="line-item link" href={link}>
      <div className="line-item__subtitle h4">
        {subtitle}
      </div>
      <div className="line-item__title h3 small">
        {title}
      </div>
    </a>
  )
}

export default LineItem
