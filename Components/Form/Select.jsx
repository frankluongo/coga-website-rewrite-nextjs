import React from 'react'
import { IconChevronDown } from '../Icons'

const Select = ({ changeHandler, dataName, selectName, id, label, modifiers, options }) => {
  return (
    <div className={`form-element select-element ${modifiers}`}>
      <label className="label-input label-input--select" htmlFor={id}>
        {label}
      </label>
      <div className="select-element-wrapper">
        <select className="select" name={selectName} id={id} onChange={changeHandler} data-for={dataName}>
          {options.map((option, index) => <option key={index} value={option.value}>{option.key}</option>)}
        </select>
        <IconChevronDown />
      </div>
    </div>
  )
}

Select.defaultProps = {
  dataName: "example",
  id: "example-select",
  label: "Default Select Field",
  modifiers: "",
  options: ["Option 1", "Option 2", "Option 3", "Option 4"],
  selectName: "Example Select"
}


export default Select
