import React, { useContext, useEffect, useRef, useState } from 'react'
import wait from "waait"

import WindowContext from "../../Context/WindowContext"
import { IconChevronDown } from "../Icons"

import "./NavigationListItem.scss"

const NavigationListItem = ({ item }) => {
  const [isOpen, toggleIsOpen] = useState(false);
  const subNavRef = useRef(null);
  const { title, url, subList } = item;
  const { width } = useContext(WindowContext)

  useEffect(showSubNav, [isOpen])

  if (subList) {
    return (
      <li className="nav-list-item">
        <button
          className={`h3 xsmall nav-list-item__link ${isOpen ? 'nav-list-item__link--sublist-open' : ''}`}
          data-href={url}
          onClick={toggleSubnav}
          onMouseEnter={handleNavMouseEnter}
          onMouseLeave={handleNavMouseLeave}
        >
          <span className="nav-list-item__text">
            {title}
          </span>
          <div className="nav-list-item__icon">
            <IconChevronDown />
          </div>
        </button>
        {isOpen && (
          <ul
            className="nav-list-item__subnav"
            data-subnav
            ref={subNavRef}
            style={{ maxHeight: 0 }}
            onMouseLeave={handleSubnavMouseLeave}
          >
            {subList.map((item, index) => (
              <li className="nav-list-item-subnav__item" key={index}>
                <a className="nav-list-item-subnav__link" href={item.url}>
                  {item.title}
                </a>
              </li>
            ))}
          </ul>
        )}
      </li>
    )
  } else {
    return (
      <li className="nav-list-item">
        <a className="h3 xsmall nav-list-item__link" href={url}>
          <span className="nav-list-item__text">
            {title}
          </span>
        </a>
      </li>
    )
  }

  function handleSubnavMouseLeave() {
    if (width >= 1280) {
      toggleSubnav();
    }
  }

  function handleNavMouseLeave() {
    if (width >= 1280) {
      const hoveredElements = Array.from(document.querySelectorAll(':hover'));
      if (!hoveredElements.find(element => element.getAttribute('data-subnav'))) {
        toggleSubnav();
      }
    }
  }

  function handleNavMouseEnter() {
    if (width >= 1280) {
      toggleSubnav()
    }
  }

  function showSubNav() {
    if (!subNavRef.current) return;
    subNavRef.current.style.maxHeight = `${subNavRef.current.scrollHeight}px`;
  }

  async function toggleSubnav() {
    if (isOpen) {
      subNavRef.current.style.maxHeight = `0px`;
      await wait(150);
    }
    toggleIsOpen(!isOpen);
  }


}

export default NavigationListItem
