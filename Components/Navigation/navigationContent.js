const navigationItems = [
  {
    title: "Session Schedule",
    url: "/session-schedule",
    subList: [
      {
        title: "Full Schedule",
        url: "/session-schedule"
      },
      {
        title: "House Schedule",
        url: "/house-schedule"
      },
      {
        title: "Senate Schedule",
        url: "/senate-schedule"
      },
    ]
  },
  {
    title: "Bills",
    url: "/bills",
    subList: [
      {
        title: "Find a Bill",
        url: "/bills"
      },
      {
        title: "All Bills",
        url: "/all-bills"
      },
      {
        title: "Bill Digest",
        url: "/bill-digest"
      },
      {
        title: "Prior Sessions",
        url: "/prior-sessions"
      },
    ]
  },
  {
    title: "Laws",
    url: "/laws",
    subList: [
      {
        title: "Colorado Laws",
        url: "/laws"
      },
      {
        title: "U.S. Constitution",
        url: "/us-constitution"
      },
      {
        title: "Colorado Constitution",
        url: "/colorado-constitution"
      },
      {
        title: "Colorado Revised Statutes",
        url: "/colorado-revised-statutes"
      },
      {
        title: "Session Laws",
        url: "/session-laws"
      },
      {
        title: "Uniform State Laws",
        url: "/uniform-state-laws"
      },
      {
        title: "Exec Agency Rules & Regulations",
        url: "/executive-agency-rules-and-regulations"
      },
    ]
  },
  {
    title: "Legislators",
    url: "/legislators",
  },
  {
    title: "Committees",
    url: "/committees",
    subList: [
      {
        title: "All Committees",
        url: "/committees"
      },
      {
        title: "Committee Information",
        url: "/committee-iformation"
      },
      {
        title: "Committee Room Policy",
        url: "/committee-room-policy"
      },
      {
        title: "Committee Archives",
        url: "/committee-archives"
      },
      {
        title: "Remote Testimony",
        url: "/remote-testimony"
      },
    ]
  },
  {
    title: "Initiatives",
    url: "/initiatives",
    subList: [
      {
        title: "Overview",
        url: "/initiatives"
      },
      {
        title: "Blue Book",
        url: "/blue-book"
      },
      {
        title: "Filed Initiatives",
        url: "/filed-initiatives"
      },
      {
        title: "Ballot Analysis",
        url: "/ballot-analysis"
      },
    ]
  },
  {
    title: "Budget",
    url: "/budget",
    subList: [
      {
        title: "Overview",
        url: "/budget"
      },
      {
        title: "JBC Documents",
        url: "/joint-budget-committee/documents"
      },
      {
        title: "Joint Budget Committee",
        url: "/joint-budget-committee"
      },
      {
        title: "Joint Technology Committee",
        url: "/joint-technology-committee"
      },
      {
        title: "Capital Development Committee",
        url: "/capital-development-committee"
      },
    ]
  },
  {
    title: "Audits",
    url: "/audits",
    subList: [
      {
        title: "Find An Audit",
        url: "/audits"
      },
      {
        title: "All Audits",
        url: "/audits"
      },
      {
        title: "Audits By Report No.",
        url: "/audits?filterby=reports"
      },
    ]
  },
  {
    title: "Session Schedule",
    url: "/session-schedule",
    subList: [
      {
        title: "Full Schedule",
        url: "/session-schedule"
      },
      {
        title: "House Schedule",
        url: "/house-schedule"
      },
      {
        title: "Senate Schedule",
        url: "/senate-schedule"
      },
    ]
  },
  {
    title: "Publications",
    url: "/publications",
    subList: [
      {
        title: "Find a Publication",
        url: "/publications"
      },
      {
        title: "All Publications",
        url: "/publications?view-all=true"
      },
      {
        title: "Senate Schedule",
        url: "/senate-schedule"
      },
    ]
  },
  {
    title: "Agencies",
    url: "/agencies",
    subList: [
      {
        title: "Agencies",
        url: "/agencies"
      },
      {
        title: "House of Representatives",
        url: "/house-of-representatives"
      },
      {
        title: "Senate",
        url: "/senate"
      },
      {
        title: "Joint Budget Committee",
        url: "/joint-budget-committee"
      },
      {
        title: "Legislative Council Staff",
        url: "/legislative-council-staff"
      },
      {
        title: "Office of Legislative Legal Services",
        url: "/office-of-legislative-legal-services"
      },
      {
        title: "Office of The State Auditor",
        url: "/office-of-the-state-auditor"
      },
    ]
  },
]

export default navigationItems
