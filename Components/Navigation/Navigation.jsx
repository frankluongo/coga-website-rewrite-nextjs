import React from 'react'

import { IconClose } from "../Icons";

import "./Navigation.scss"
import SiteLogo from '../SiteLogo';
import SearchForm from '../SearchForm';
import navigationItems from "./navigationContent"
import NavigationListItem from './NavigationListItem';

const Navigation = ({ closeAction, modifiers, navRef }) => {
  return (
    <nav className={`website-navigation ${modifiers}`} aria-label="Website Navigation" ref={navRef}>
      <div className="website-navigation__header">
        <SiteLogo modifiers="website-navigation-header__logo" />
        <div className="website-navigation-header__close">
          <button
            className="website-navigation-header__close-button"
            aria-label="Close Website Navigation"
            onClick={closeAction}>
            <IconClose />
          </button>
        </div>
      </div>
      <div className="website-navigation__search">
        <SearchForm modifiers="search-form--navigation" searchId="navigation-search" />
      </div>
      <ul className="navigation-list">
        {navigationItems.map((item, index) => <NavigationListItem item={item} key={index} />)}
      </ul>
    </nav>
  )
}

Navigation.defaultProps = {
  modifiers: ""
}

export default Navigation
