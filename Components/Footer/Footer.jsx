import React from 'react'

import "./Footer.scss"

import data from "./footerData"
import SiteLogo from '../SiteLogo'

const Footer = () => {
  return (
    <footer className="footer">
      <section className="footer__container container">
        <div className="footer-container__section footer__contact-info footer-contact-info">
          <SiteLogo modifiers="footer-contact-info__logo" />
          <address className="footer-contact-info__address">
            {data.address}
          </address>
          <a className="footer-contact-info__email link" href={`mailto:${data.commentsEmail}`} rel="noopener noreferrer" target="_blank">
            {data.commentsEmail}
          </a>
        </div>
        {data.linkLists.map((list, index) => (
          <div className="footer-container__section footer__link-list footer-link-list" key={index}>
            <h3 className="footer-link-list__heading h3">{list.heading}</h3>
            <ul className="footer-link-list__list">
              {list.links.map((link, index) => (
                <li className="footer-link-list__item" key={index}>
                  <a className="footer-link-list__link link" href={link.url} target="_blank" rel="noopener noreferrer">
                    {link.title}
                  </a>
                </li>
              ))}
            </ul>
          </div>
        ))}
      </section>
    </footer>
  )
}

export default Footer
