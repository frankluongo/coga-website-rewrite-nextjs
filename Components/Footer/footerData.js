import React from "react"

const footerData = {
  address: (
    <>
      Colorado General Assembly<br />
      200 E. Colfax Avenue<br />
      Denver, CO 80203
    </>
  ),
  commentsEmail: 'comments.ga@state.co.us',
  linkLists: [
    {
      heading: 'Resources & Information',
      links: [
        {
          url: '/',
          title: 'Rules & Regulations of Executive Agencies'
        },
        {
          url: '/',
          title: 'Colorado Open Records Act Maximum Hourly Research and Retrieval Fee'
        },
        {
          url: '/',
          title: 'Transparency Online Project'
        },
        {
          url: '/',
          title: 'State Home'
        },
        {
          url: '/',
          title: 'Legislative Workplace Study'
        },
        {
          url: '/',
          title: 'Legislative Resources & Requirements'
        },
        {
          url: '/',
          title: 'Salaries for Legislators, Statewide Elected Officials, and County Officers'
        },
      ]
    },
    {
      heading: 'Policies',
      links: [
        {
          url: '/',
          title: 'Capitol Security Protocol'
        },
        {
          url: '/',
          title: 'Open Records Requests & Policy'
        },
        {
          url: '/',
          title: 'Privacy Policy'
        },
        {
          url: '/',
          title: 'Public Wi-Fi'
        },
        {
          url: '/',
          title: 'Services for Persons with Disabilities'
        },
        {
          url: '/',
          title: 'Workplace Harassment Policy'
        },
      ]
    },
    {
      heading: 'For Legislators & Staff',
      links: [
        {
          url: '/',
          title: 'Ethics Tutorial'
        },
        {
          url: '/',
          title: 'IT Login'
        },
        {
          url: '/',
          title: 'New Member Orientation'
        },
        {
          url: '/',
          title: 'Social Calendar'
        },
        {
          url: '/',
          title: 'House and Senate Rules'
        },
      ]
    },
  ]
}

export default footerData
