const utilityNavLinks = [
  {
    url: '/visit-and-learn',
    title: "Visit & Learn"
  },
  {
    url: '/find-my-legislator',
    title: "Find My Legislator"
  },
  {
    url: '/watch-and-listen',
    title: "Watch & Listen"
  },
]

export default utilityNavLinks
