import React from 'react'

import HeaderNavigation from './HeaderNavigation'
import SiteLogo from '../SiteLogo'

import "./Header.scss"
import HeaderUtilityNav from './HeaderUtilityNav'

const Header = () => {
  return (
    <header className="header">
      <section className="header__container container">
        <SiteLogo modifiers="header__logo" />
        <HeaderUtilityNav />
        <HeaderNavigation />
      </section>
    </header>
  )
}

export default Header
