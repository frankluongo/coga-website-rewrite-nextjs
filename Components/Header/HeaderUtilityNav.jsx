import React from 'react'

import SearchForm from '../SearchForm/SearchForm'


import utilityNavLinks from "./utilityNavLinks";
import "./HeaderUtilityNav.scss"

const HeaderUtilityNav = () => {
  return (
    <section className="header-utility-nav header__utility-nav">
      <ul className="header-utility-nav__list">
        {utilityNavLinks.map((item, index) => (
          <li className="header-utility-nav-list__item" key={index}>
            <a className="header-utility-nav-list-item__link" href={item.url}>
              {item.title}
            </a>
          </li>
        ))}
      </ul>
      <SearchForm modifiers="search-form--utility-nav" searchId="util-header-search" />
    </section>
  )
}

export default HeaderUtilityNav
