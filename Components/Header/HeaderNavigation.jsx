import React, { useContext, useEffect, useRef, useState } from 'react'
import wait from 'waait';

import { IconBurger } from "../Icons";
import Navigation from '../Navigation';
import WindowContext from "../../Context/WindowContext"

import "./HeaderNavigation.scss"

const ACTIVE_CLASS = "website-navigation--active"
const NAV_ACTIVE = "nav-active";

const HeaderNavigation = () => {
  let isMounted = true;
  const navRef = useRef(null);
  const [isOpen, toggleNavOpen] = useState(false)
  const { width } = useContext(WindowContext)

  useEffect(handleNavToggle, [isOpen])
  useEffect(handleNavOpenOnWindowResize, [width])

  return (
    <div className="header-navigation header__navigation">
      <button
        className="header-navigation__open-button"
        aria-label="Open Website Navigation"
        onClick={showNav}
        title="Open Website Navigation"
      >
        <IconBurger />
      </button>
      {isOpen && (
        <Navigation navRef={navRef} closeAction={hideNav} />
      )}
      <Navigation modifiers="website-navigation--desktop" />
    </div>
  )

  function handleNavOpenOnWindowResize() {
    if (isOpen && isMounted) {
      hideNav()
    }
    return () => {
      isMounted = false;
    };
  }

  function handleNavToggle() {
    if (!navRef.current || !isMounted) return;
    document.body.classList.add(NAV_ACTIVE);
    navRef.current.classList.add(ACTIVE_CLASS);
    return () => {
      isMounted = false;
    }
  }

  async function hideNav() {
    navRef.current.classList.remove(ACTIVE_CLASS);
    await wait(150);
    document.body.classList.remove(NAV_ACTIVE);
    toggleNavOpen(false)
  }

  function showNav() {
    toggleNavOpen(true);
  }
}

export default HeaderNavigation
