import React from 'react'

import "./NotificationBar.scss"

const NotificationBar = ({ notificationContent }) => {
  return (
    <section aria-label="Sitewide Notice" className="notification-bar">
      {notificationContent}
    </section>
  )
}

NotificationBar.defaultProps = {
  notificationContent: 'Default Text'
}

export default NotificationBar
