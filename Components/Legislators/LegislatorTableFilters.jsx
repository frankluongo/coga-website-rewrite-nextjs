import React from 'react'

import { Select } from '../Form';

const LegislatorTableFilters = ({ handleFilterChange, handleSortChange }) => {
  return (
    <section className="legislators-table__filters">
      <Select
        changeHandler={handleFilterChange}
        id="chamber-select"
        dataName="chamber"
        selectName="Chamber Select"
        label="Chamber"
        modifiers="legislators-table-filters__filter"
        options={[
          {
            value: "all",
            key: "All"
          },
          {
            value: "house",
            key: "House"
          },
          {
            value: "senate",
            key: "Senate"
          },
        ]}
      />
      <Select
        changeHandler={handleFilterChange}
        id="party-select"
        dataName="party"
        selectName="Party Select"
        label="Party"
        modifiers="legislators-table-filters__filter"
        options={[
          {
            value: "all",
            key: "All"
          },
          {
            value: "democrat",
            key: "Democrat"
          },
          {
            value: "republican",
            key: "Republican"
          },
          {
            value: "unaffiliated",
            key: "Unaffiliated"
          },
        ]}
      />
      <Select
        changeHandler={handleSortChange}
        id="sort-select"
        dataName="sort"
        selectName="Sort Select"
        label="Sort By"
        modifiers="legislators-table-filters__filter"
        options={[
          {
            value: "name-descending",
            key: "Name Descending"
          },
          {
            value: "name-ascending",
            key: "Name Ascending"
          },
          {
            value: "district-number-descending",
            key: "District Number Descending"
          },
          {
            value: "district-number-ascending",
            key: "District Number Ascending"
          },
        ]}
      />
      <Select
        changeHandler={() => { console.log('hi') }}
        id="session-select"
        dataName="session"
        selectName="View By Session"
        label="View By Session"
        modifiers="legislators-table-filters__filter"
        options={[
          {
            value: "regular-session-2020",
            key: "2020 Regular Session"
          },
          {
            value: "regular-session-2019",
            key: "2019 Regular Session"
          },
          {
            value: "regular-session-2018",
            key: "2018 Regular Session"
          },
          {
            value: "extraordinary-session-2017",
            key: "2017 Extraordinary Session"
          },
          {
            value: "regular-session-2017",
            key: "2017 Regular Session"
          },
          {
            value: "regular-session-2016",
            key: "2016 Regular Session"
          },
        ]}
      />
    </section>
  )
}

export default LegislatorTableFilters
