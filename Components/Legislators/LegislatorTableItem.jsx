import React from 'react'
import { TableData, TableRow } from '../Table';

const LegislatorTableItem = ({ legislator }) => {
  const { chamber, district, political_affiliation: party, contact_phone, contact_email, id, last_name, first_name } = legislator;

  return (
    <TableRow type="body">
      <TableData>
        {chamber === 'House' ? 'Representative' : 'Senator'}
      </TableData>
      <TableData>
        <a className="link" href={`/legislators/${id}`}>
          {last_name}, {first_name}
        </a>
      </TableData>
      <TableData>
        {district}
      </TableData>
      <TableData>
        {party}
      </TableData>
      <TableData>
        <a className="link" href={`tel:${contact_phone}`}>
          {contact_phone}
        </a>
      </TableData>
      <TableData>
        <a className="link" href={`mailto:${contact_email}`} target="_blank" rel="noopener noreferrer">
          {contact_email}
        </a>
      </TableData>
    </TableRow>
  )
}

export default LegislatorTableItem
