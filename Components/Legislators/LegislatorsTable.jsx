import React, { useEffect, useState } from 'react'

import { Table, TableHead, TableRow, TableHeading, TableBody } from "../Table"

import "./LegislatorsTable.scss"
import LegislatorTableItem from './LegislatorTableItem';
import LegislatorTableFilters from './LegislatorTableFilters';

const LegislatorsTable = ({ headings, legislators }) => {
  const [activeLegislators, updateActiveLegislators] = useState(legislators)
  const [activeFilters, updateActiveFilters] = useState({
    chamber: 'all',
    party: 'all'
  });
  const [activeSort, updateActiveSort] = useState('name-descending')

  useEffect(handleActiveLegislatorsChange, [activeFilters, activeSort])

  return (
    <div className="legislators-table">
      <LegislatorTableFilters
        handleFilterChange={handleFilterChange}
        handleSortChange={handleSortChange}
      />
      <div className="legislators-table__table-wrapper">
        <section className="legislators-table__table">
          <Table>
            <TableHead>
              <TableRow type="head">
                {headings.map((heading, index) => (
                  <TableHeading key={index}>
                    {heading}
                  </TableHeading>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {activeLegislators.map((legislator, index) => (
                <LegislatorTableItem legislator={legislator} key={index} />
              ))}
            </TableBody>
          </Table>
        </section>
      </div>
    </div>
  )

  //
  // Handlers
  //
  function handleFilterChange(e) {
    const select = e.currentTarget;
    const selectFor = select.dataset.for;
    const selectedOption = select.options[select.options.selectedIndex].value;
    const newActiveFilters = { ...activeFilters, [selectFor]: selectedOption };
    updateActiveFilters(newActiveFilters)
  }

  function handleSortChange(e) {
    const select = e.currentTarget;
    const selectedOption = select.options[select.options.selectedIndex].value;
    updateActiveSort(selectedOption)
  }

  function handleActiveLegislatorsChange() {
    const newActiveLegislators = legislators.filter(filterActiveLegislator)
    const sortedActiveLegislators = sortActiveLegislators(newActiveLegislators)
    updateActiveLegislators(sortedActiveLegislators)
  }

  //
  // Actions
  //
  function sortActiveLegislators(legislators) {
    switch (activeSort) {
      case 'name-descending':
        legislators.sort(sortByName)
        break;
      case 'name-ascending':
        legislators.sort(sortByName).reverse()
        break;
      case 'district-number-descending':
        legislators.sort(sortByDistrict)
        break;
      case 'district-number-ascending':
        legislators.sort(sortByDistrict).reverse()
        break;
      default:
        legislators
    }
    return legislators
  }

  function filterActiveLegislator(legislator) {
    const legChamber = legislator.chamber.toLowerCase();
    const legParty = legislator.political_affiliation.toLowerCase();
    const curChamber = checkForAll(activeFilters.chamber, legChamber);
    const curParty = checkForAll(activeFilters.party, legParty);

    return legChamber === curChamber && legParty === curParty;
  }
  //
  // Helpers
  //
  function checkForAll(filter, match) {
    return filter === 'all' ? match : filter;
  }

  function sortByName(firstEl) {
    return firstEl.last_name
  }
  function sortByDistrict(firstEl, secondEl) {
    return parseInt(secondEl.district) - parseInt(firstEl.district)
  }
}

export default LegislatorsTable
