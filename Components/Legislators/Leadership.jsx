import React from 'react'

import "./Leadership.scss"

const Leader = ({ leader }) => (
  <li className="leadership-list__item">
    <a className="leadership-list__link leadership-list-link" href={leader.url} target="_blank" rel="noopener noreferrer">
      <small className="leadership-list-link__position">{leader.position}</small>
      <span className="leadership-list-link__name h3 small">
        {leader.name}
      </span>
    </a>
  </li>
)

const Leadership = ({ legislators }) => {
  const houseLeaders = getLeadership(legislators, 'House')
  const senateLeaders = getLeadership(legislators, 'Senate')

  return (
    <section className="leadership">
      <div className="leadership__content-wrapper">
        <div className="leadership__list-wrapper">
          <h3 className="leadership__heading h3">House Leadership</h3>
          <ul className="leadership__list">
            {houseLeaders.map((leader, index) => (
              <Leader leader={leader} key={index} />
            ))}
          </ul>
        </div>
        <div className="leadership__list-wrapper">
          <h3 className="leadership__heading h3">Senate Leadership</h3>
          <ul className="leadership__list">
            {senateLeaders.map((leader, index) => (
              <Leader leader={leader} key={index} />
            ))}
          </ul>
        </div>
      </div>
    </section>
  )

  //
  // Actions
  //
  function getLeadership(legislators, chamber) {
    const houseLegsWithLeadershipRoles = legislators.filter((legislator) => getLeadersInChamber(legislator, chamber))
    const leaders = houseLegsWithLeadershipRoles.map(buildLeadersArray)
    return leaders
  }
  //
  // Filter Functions
  //
  function getLeadersInChamber(leg, chamber) {
    return leg.chamber === chamber && leg.leadership_positions.length > 0
  }

  function buildLeadersArray(leg) {
    const { first_name, last_name, url, leadership_positions: positions } = leg
    const position = positions[positions.length - 1].name;

    return {
      url,
      name: `${first_name} ${last_name}`,
      position
    }
  }
}

export default Leadership
