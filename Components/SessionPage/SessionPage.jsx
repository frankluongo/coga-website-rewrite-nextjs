import React from 'react'
import { Select } from '../Form'

import "./SessionPage.scss"

const SessionPage = () => {
  return (
    <div className="session-page">
      <div className="session-page__heading">
        <h2 className="session-page-heading__heading h3">
          Week of Mar 8, 2020 - Mar 14 2020
        </h2>
        <a className="link" href="/">
          View Next Week
        </a>
      </div>
      <div className="session-page__sorts">
        <div className="session-page-sorts__sort">
          <Select
            dataName="view"
            id="view-select"
            label="View"
            options={[
              {
                key: "Daily",
                value: "daily"
              },
              {
                key: "Weekly",
                value: "weekly"
              },
            ]}
          />
        </div>
        <div className="session-page-sorts__sort">
          <Select
            dataName="chamber"
            id="chamber-select"
            label="Chamber"
            options={[{ key: "Both", value: "both" }, { key: "House", value: "house" }, { key: "Senate", value: "senate" }]}
            selectName="Chamber Select"
          />
        </div>
      </div>
    </div>
  )
}

export default SessionPage
