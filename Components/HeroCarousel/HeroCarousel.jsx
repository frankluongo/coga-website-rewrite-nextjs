import React, { useState } from 'react'

import sampleData from "./heroCarouselSampleData"
import HeroSlide from './HeroSlide'

import { IconChevronLeft, IconChevronRight } from "../Icons"

import "./HeroCarousel.scss"

const HeroCarousel = ({ slides }) => {
  const [activeIndex, updateActiveIndex] = useState(0);
  const limit = slides.length - 1;

  return (
    <section className="hero-carousel">
      <ul className="hero-carousel__slides hero-carousel-slides">
        {slides.map((slide, index) => (
          <HeroSlide
            slide={slide}
            key={index}
            isActive={activeIndex === index}
            isPrev={activeIndex === index - 1}
            isNext={activeIndex === index + 1}
          />
        )
        )}
      </ul>
      <div className="hero-carousel__controls">
        <div className="hero-carousel-controls__dots">
          {slides.map((_, index) => (
            <button
              className={`hero-carousel__dot ${index === activeIndex ? 'hero-carousel__dot--active' : ''}`}
              key={index}
              onClick={() => updateActiveIndex(index)}
              arial-label={`View Slide ${index + 1}`}
            >
              <div className="hero-carousel-dot__circle" />
            </button>
          ))}
        </div>
        <div className="hero-carousel-controls__arrows">
          <button
            className="hero-carousel-controls__arrow hero-carousel-controls__arrow--prev"
            onClick={() => updateActiveIndex(getPrevSlide)}
            aria-label="Previous Slide"
          >
            <IconChevronLeft />
          </button>
          <button
            className="hero-carousel-controls__arrow hero-carousel-controls__arrow--next"
            onClick={() => updateActiveIndex(getNextSlide)}
            aria-label="Next Slide"
          >
            <IconChevronRight />
          </button>
        </div>
      </div>
    </section>
  )

  function getPrevSlide() {
    return activeIndex - 1 < 0 ? limit : activeIndex - 1;
  }
  function getNextSlide() {
    return activeIndex + 1 > limit ? 0 : activeIndex + 1;
  }
}

HeroCarousel.defaultProps = {
  slides: sampleData
}

export default HeroCarousel
