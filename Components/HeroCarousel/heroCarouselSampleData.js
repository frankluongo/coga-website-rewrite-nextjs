const heroCarouselSampleData = [
  {
    title: "The 72nd General Assembly",
    description: "The First Regular Session of the Seventy-second General Assembly adjourned on May 3, 2019 and the Second Regular Session will convene on January 8, 2020.",
    callToAction: {
      url: "/",
      title: "Call to Action"
    },
    imageUrl: "https://placekitten.com/g/1440/900"
  },
  {
    title: "Speaker KC Becker",
    description: "House Speaker Becker resides in Boulder and represents District 13.",
    callToAction: {
      url: "/",
      title: "Call to Action"
    },
    imageUrl: "https://placekitten.com/g/1441/900"
  },
  {
    title: "President Leroy M. Garcia",
    description: "Senate President Garcia resides in Pueblo and represents District 3.",
    callToAction: {
      url: "/",
      title: "Call to Action"
    },
    imageUrl: "https://placekitten.com/g/1442/900"
  },
]

export default heroCarouselSampleData
