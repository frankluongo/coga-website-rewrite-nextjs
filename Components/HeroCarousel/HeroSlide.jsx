import React from 'react'

import "./HeroSlide.scss"

const HeroSlide = ({ isActive, isPrev, isNext, slide }) => {
  return (
    <li
      className="hero-carousel-slides__slide hero-carousel-slide"
      data-active={isActive}
      data-prev={isPrev}
      data-next={isNext}
    >
      <div className="hero-carousel-slide__image" style={{ backgroundImage: `url(${slide.imageUrl}` }} />
      <section className="hero-carousel-slide__text">
        <div className="h1 xsmall">
          {slide.title}
        </div>
        <p className="hero-carousel-slide__description">
          {slide.description}
        </p>
        <a href={slide.callToAction.url} className="hero-carousel-slide__button button default primary">
          {slide.callToAction.title}
        </a>
      </section>
    </li>
  )
}

export default HeroSlide
